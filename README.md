# EOS project landing page
![Open Source Love png2](https://badges.frapsoft.com/os/v2/open-source.png?v=103)
![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)

Demo: https://eosdesignsystem.com

### Installation

1. `git clone https://gitlab.com/SUSE-UIUX/eos-landing.git`
2. `cd eos-landing`
3. Install all dependencies. This will also install the FE vendors in a followup script:
  ```
    npm install
  ```
4. Intall nodemon to run the watchers `npm i -g nodemon`
5. If you are developing this site, run the watchers with `npm start`
6. Generate vendors folder: `npm run build:vendors`
7. In MacOS, run the server in a new terminal with:

    ```
      npm run serve:macos
    ```
    It will build your assets and serve in `localhost:8000`

8. In Linux, access the dist folder with `cd dist` and start a server:

    ```
      python3 -m http.server
    ```

    or

    ```
      python2 -m SimpleHTTPServer
    ```

    Depending on the version of Python available in your system

### Running lints:

Test all:
`npm run test:all`

Sass:
`npm run test:sass`

JS:
`npm run test:js`

Pug:
`npm run test:pug`

Unit testing:
`npm run test:unit`

# Learn more about the EOS Design System

* [EOS Design System](https://www.eosdesignsystem.com/)

* [EOS Icons](icons.eosdesignsystem.com/)

* [Follow us on Twitter](https://twitter.com/eosdesignsystem)

* [Join us in Slack](https://slack.eosdesignsystem.com)

# Our "thank you" section

### Tested for every browser in every device

Thanks to [Browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.
